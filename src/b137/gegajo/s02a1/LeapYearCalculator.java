package b137.gegajo.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args){
        System.out.println("Leap Year Calculator\n");

        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is your first name?\n");
        String firstName = appScanner.nextLine();
        System.out.println("Hello, " + firstName + "!\n");

        //Activity:

        System.out.println("Please input year\n");
        int year = appScanner.nextInt();
        boolean isLeap = false;

        if (year % 4 == 0 ){
            if (year % 100 == 0){
                if(year % 400 == 0)
                    isLeap = true;
                else
                    isLeap = false;
            }
            else
                isLeap = true;
        }
        else
            isLeap = false;

        if(isLeap)
            System.out.println( year + " " + "is a leap year");
        else
            System.out.println( year + " " + "is not a leap year");
    }
}
